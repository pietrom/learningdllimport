# Compilazione
## Su macchina Linux
### Pre-requisito
```bash
sudo apt install -y gcc-mingw-w64-i686
```
### Target Linux
```bash
gcc -shared -o subdir/add.so add.c
```
### Target Windows 64 bit
```bash
x86_64-w64-mingw32-gcc -shared -o subdir/calculator-x64.dll  -Wl,--subsystem,windows calculator.c
```
### Target Windows 32 bit
```bash
i686-w64-mingw64-gcc -shared -o subdir/calculator.dll  -Wl,--subsystem,windows64 calculator.c
```