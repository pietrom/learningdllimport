﻿using System.Runtime.InteropServices;

public class Program {
    public static void Main() {
        var calc = GetCalculator();
        Console.WriteLine(calc.Add(11, 19));
        Console.WriteLine(calc.Hash("pietrom", 19));
        var randomData = calc.RandomData(19);
        Console.WriteLine("Returned");
        Console.WriteLine($"{randomData.Text} -- {randomData.Value}");
    }

    static Calculator GetCalculator() {
        return Environment.OSVersion.Platform switch {
            PlatformID.Unix => new UnixCalculator(),
            PlatformID.Win32Windows or PlatformID.Win32NT => new WinCalculator(),
            _ => throw new Exception($"Unsupported platform {Environment.OSVersion.Platform}")
        };
    }
}

public interface Calculator {
    public int Add(int a, int b);

    public int Hash(string text, int value);
    
    public DataContainer RandomData(int n);
}

internal class UnixCalculator : Calculator {
    public int Add(int a, int b) {
        return UnixLibraryWrapper.Add(a, b);
    }

    public int Hash(string text, int value) {
        var data = new DataContainer { Text = text, Value = value };
        return UnixLibraryWrapper.Hash(ref data);
    }

    public DataContainer RandomData(int n) {
        var result = new DataContainer { Text = "initial", Value = -1};
        UnixLibraryWrapper.Random(n, ref result);
        return result;
    }
}

public struct DataContainer {
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
    public string Text;

    public int Value;
}

public class UnixLibraryWrapper {
    [DllImport("subdir/calculator.so", EntryPoint = "add", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
    public static extern int Add(int x, int y);
    
    [DllImport("subdir/calculator.so", EntryPoint = "hash_data", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
    public static extern int Hash(ref DataContainer data);  
    
    [DllImport("subdir/calculator.so", EntryPoint = "random_data", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
    public static extern int Random(int n, ref DataContainer data);
}

internal class WinCalculator : Calculator {
    public int Add(int a, int b) {
        return WinLibraryWrapper.Add(a, b);
    }

    public int Hash(string text, int value) {
        var data = new DataContainer { Text = text, Value = value };
        return WinLibraryWrapper.Hash(ref data);
    }

    public DataContainer RandomData(int n) {
        var result = new DataContainer { Text = "initial", Value = -1};
        WinLibraryWrapper.Random(n, ref result);
        return result;
    }
}

public class WinLibraryWrapper {
    [DllImport("subdir/calculator-x64.dll", EntryPoint = "add", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
    public static extern int Add(int x, int y);
    
    [DllImport("subdir/calculator-x64.dll", EntryPoint = "hash_data", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
    public static extern int Hash(ref DataContainer data);  
    
    [DllImport("subdir/calculator-x64.dll", EntryPoint = "random_data", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
    public static extern int Random(int n, ref DataContainer data);
}
