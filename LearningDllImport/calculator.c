/* add.c

Demonstrates creating a DLL with an exported function in a flexible and
   elegant way.
*/

#include "calculator.h"
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

int ADDCALL add(int a, int b)
{
    return (a + b) * 3;
}

int ADDCALL hash_data(DataContainer *data)
{
    return (*data).value + strlen((*data).text);
}

void ADDCALL random_data(int n, DataContainer *data)
{
    printf("0 Here %s %d\n", (*data).text, (*data).value);
    srand(time(NULL));   // Initialization, should only be called once.
    int r = rand();
    strcpy((*data).text, "pietrom");
    (*data).value = r;
    printf("1 Here %s %d\n", (*data).text, (*data).value);
}
