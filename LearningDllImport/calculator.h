#ifdef ADD_EXPORTS
    #define ADDAPI __attribute__((dllexport))
#else
    #define ADDAPI __attribute__((dllexport))
#endif

/* Define calling convention in one place, for convenience. */
#define ADDCALL __attribute__((__cdecl__))


/* Make sure functions are exported with C linkage under C++ compilers. */

/* Declare our Add function using the above definitions. */
struct data
{
    char* text;
    int value;
};

#define DATA_TEXT_MAX_SIZE  100 

typedef struct DataContainerStruct {
    char text[DATA_TEXT_MAX_SIZE];
    int value;
} DataContainer;

ADDAPI int ADDCALL add(int a, int b);
ADDAPI int ADDCALL hash_data(DataContainer *data);
ADDAPI void ADDCALL random_data(int n, DataContainer *data);

